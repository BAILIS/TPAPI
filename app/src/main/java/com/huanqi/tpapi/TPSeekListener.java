package com.huanqi.tpapi;

import com.huanqi.tpapi.cybergarage.upnp.Device;


public interface TPSeekListener {
    public final static int PAUSED_PLAYBACK = 0;//暂停播放
    public final static int PLAYING = 1;//开始播放
    public final static int STOPPED = 2;//停止播放
    public final static int TRANSITIONING = 3;//过渡
    public final static int NO_MEDIA_PRESENT = 4;

    void TPSeekMessage(String name,int seek,int max);
    void TPState(int state);
    void TPDeviceMessage(Device device, int state);
    void TPEventMessage(String EventMessage);
}
