package com.huanqi.tpapi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.huanqi.hqw.Utils.HQWLogUtil;
import com.huanqi.tpapi.cybergarage.upnp.Device;
import com.huanqi.tpapi.cybergarage.upnp.Service;

import com.huanqi.tpapi.cybergarage.upnp.Action;
import com.huanqi.tpapi.cybergarage.upnp.Device;
import com.huanqi.tpapi.cybergarage.upnp.Service;
import com.huanqi.tpapi.cybergarage.xml.Node;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ItemOnclick {
    Service service;

    String url = "https://alpan.51huanqi.cn/d/%E5%BD%B1%E8%A7%86%E7%AB%99%E7%82%B9/%E5%8A%A8%E6%BC%AB/%E5%8A%A8%E6%BC%AB2/%E7%81%B5%E5%89%91%E5%B0%8A/214.mp4";
    TPMovieApi tpMovieApi;

    RecyclerView rcv_player_touping;
    MoviePlayerToupingAdapter moviePlayerToupingAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rcv_player_touping = findViewById(R.id.rcv_player_touping);
        moviePlayerToupingAdapter = new MoviePlayerToupingAdapter(this);
        rcv_player_touping.setLayoutManager(WeightUtil.llm(this, LinearLayoutManager.VERTICAL));
        rcv_player_touping.setAdapter(moviePlayerToupingAdapter);
        moviePlayerToupingAdapter.setItemOnclick(this);

        tpMovieApi = new TPMovieApi();
        tpMovieApi.oncreate();
        tpMovieApi.setTpSeekListener(new TPSeekListener() {
            @Override
            public void TPSeekMessage(String name, int seek, int max) {
                HQWLogUtil.logi("状态", seek + "");
            }

            @Override
            public void TPState(int state) {
                HQWLogUtil.logi("状态", state + "");
            }

            @Override
            public void TPDeviceMessage(Device device, int state) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Beantouping beantouping = new Beantouping();
                        beantouping.setName(device.getFriendlyName());
                        beantouping.setDevice(device);
                        moviePlayerToupingAdapter.addData(beantouping);
                    }
                });

            }

            @Override
            public void TPEventMessage(String EventMessage) {

            }
        });
    }


    @Override
    public void callback(int type, RecyclerView.ViewHolder holder, List<?> lists, int position) {
        if (type == ItemOnclick.TOUPING) {
            List<Beantouping> beantoupingList = (List<Beantouping>) lists;
            Device device=beantoupingList.get(position).getDevice();
            tpMovieApi.addSeekListener(tpMovieApi.DevicetoService(device));
            tpMovieApi.addStateListener(tpMovieApi.DevicetoService(device));
//            tpMovieApi.addSubscribeListener(tpMovieApi.DevicetoService(device));
            tpMovieApi.startTP(tpMovieApi.DevicetoService(device),"名称啊啊啊啊",url);
        }
    }
}