package com.huanqi.tpapi;


import com.huanqi.tpapi.cybergarage.upnp.Device;

public class Beantouping {
    String name;
    Device device;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
