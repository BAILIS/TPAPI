package com.huanqi.tpapi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

public class MoviePlayerToupingAdapter extends RecyclerView.Adapter {
    Context context;
    List<Beantouping> beantoupingList=new ArrayList<>();

    ItemOnclick itemOnclick;

    public void setItemOnclick(ItemOnclick itemOnclick) {
        this.itemOnclick = itemOnclick;
    }

    public MoviePlayerToupingAdapter(Context context){
        this.context=context;
    }

    public void addData(Beantouping beantouping){
        beantoupingList.add(beantouping);
        notifyDataSetChanged();
    }

    public void remoteData(String name){
        for (int i = 0; i < beantoupingList.size(); i++) {
            if (beantoupingList.get(i).getName().equals(name)){
                beantoupingList.remove(beantoupingList.get(i));
                notifyDataSetChanged();
            }
        }
    }

    public void remoteall(){
        beantoupingList.removeAll(beantoupingList);
        notifyDataSetChanged();

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_player_touping,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        ViewHolder viewHolder=(ViewHolder)holder;
        viewHolder.tv_item_player_touping_text.setText(beantoupingList.get(position).getName());
        viewHolder.tv_item_player_touping_text.setOnClickListener(v -> {
            itemOnclick.callback(ItemOnclick.TOUPING,holder,beantoupingList,position);
        });
    }

    @Override
    public int getItemCount() {
        return beantoupingList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_player_touping_text;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_item_player_touping_text=itemView.findViewById(R.id.tv_item_player_touping_text);
        }
    }
}
