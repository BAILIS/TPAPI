/******************************************************************
*
*	CyberHTTP for Java
*
*	Copyright (C) Satoshi Konno 2002
*
*	File: HTTPRequestListener.java
*
*	Revision;
*
*	12/13/02
*		- first revision.
*	
******************************************************************/

package com.huanqi.tpapi.cybergarage.http;

public interface HTTPRequestListener
{
	public void httpRequestRecieved(HTTPRequest httpReq);
}
