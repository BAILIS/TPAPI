package com.huanqi.tpapi;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public interface ItemOnclick {
    public final static int BEISU=-1;
    public final static int MOVIE=0;
    public final static int JUJI=1;
    public final static int ZIMU=2;
    public final static int QITA=3;
    public final static int VIDEO_JUJI=4;
    public final static int VIDEO_ZIMU=5;
    public final static int TOUPING=6;
    void callback(int type,RecyclerView.ViewHolder holder, List<?> lists,int position);
}
