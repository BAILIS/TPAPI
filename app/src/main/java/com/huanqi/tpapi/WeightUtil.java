package com.huanqi.tpapi;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;

public class WeightUtil {

    public static LinearLayoutManager llm(Context context,int type){
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        if (type==LinearLayoutManager.HORIZONTAL){
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        }else  if (type==LinearLayoutManager.VERTICAL){
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        }
        return linearLayoutManager;
    }

}
