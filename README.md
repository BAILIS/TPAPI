# TPAPI

#### 介绍
简单进行视频投屏


#### 安装教程  ![最新版本](https://jitpack.io/v/com.gitee.BAILIS/TPAPI.svg "version")
Gradle

```
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```

```
 implementation 'com.gitee.BAILIS:TPAPI:Tag'
```
Maven

```
	<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>
```

```
<dependency>
	    <groupId>com.gitee.BAILIS</groupId>
	    <artifactId>TPAPI</artifactId>
	    <version>Tag</version>
	</dependency>
```


#### 使用说明

1.  直接调用，填写设备service 请先从setTpSeekListener中TPDeviceMessage获取搜索到的device,在获取service

```
        TPMovieApi tpMovieApi = new TPMovieApi();
        tpMovieApi.oncreate();
        tpMovieApi.setseek(Service service,int seek);//设置视频进度 单位 24*60*60*1000
        tpMovieApi.addSeekListener();//激活投屏设备视频进度监听器
        tpMovieApi.DevicetoService(Device device);//Device获取Service
        tpMovieApi.play(Service service);//操作投屏设备播放
        tpMovieApi.pause(Service service);//操作投屏设备暂停
        tpMovieApi.stop(Service service);//操作投屏设备停止
```
2.setTpSeekListener回调信息

```
tpMovieApi.setTpSeekListener(new TPSeekListener() {
            @Override
            public void TPSeekMessage(String name,int seek, int max) {
                HQWLogUtil.logi("投屏设备的视频名称及进度和总长度", name+seek+"      "+max);
            }

            @Override
            public void TPState(int state) {//设备状态
               switch (state){
                   case TPMovieApi.PLAYING:
                       HQWLogUtil.logi("投屏设备状态","播放"+tpapi.TPSTATE);
                       break;
                   case TPMovieApi.PAUSED_PLAYBACK:
                       HQWLogUtil.logi("投屏设备状态","暂停");
                       break;
                   case TPMovieApi.STOPPED:
                       HQWLogUtil.logi("投屏设备状态","停止");
                       break;
               }
            }
            @Override
            public void TPDeviceMessage(Device device, int state) {//设备控制器回调
                switch (state) {
                    case TPMovieApi.AddDevice://device上线
                        break;
                    case TPMovieApi.RemoveDevice://device离线
                        break;
                }
            }
            @Override
            public void TPEventMessage(String EventMessage) {//报错信息
            }
        });

```


